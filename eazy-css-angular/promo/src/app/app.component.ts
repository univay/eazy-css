import {Component, OnInit} from '@angular/core';

interface Flex {
  d: string,
  w: string,
  j: string,
  a: string,
  ac: string
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  public flexAttr: string = '';
  public flex: Flex = {
    d: '',
    w: '',
    j: '',
    a: '',
    ac: ''
  };

  public updFlex(variable, value: string): void {
    this.flex[variable] = value;
    let flexEl = document.getElementById('flex');
    this.flexAttr = Object.values(this.flex)
      .map(attr => attr.split('='))
      .map(attr => attr[1] ? `${attr[0]}="${attr[1]}"` : attr[0])
      .join(' ').trim();
    flexEl.removeAttribute('flex-d');
    flexEl.removeAttribute('flex-w');
    flexEl.removeAttribute('flex-j');
    flexEl.removeAttribute('flex-a');
    flexEl.removeAttribute('flex-ac');
    Object.values(this.flex).map(attr => attr.split('=')).forEach(attr => {
      if (attr[1]) {
        flexEl.setAttribute(attr[0], attr[1]);
      } else if (attr[0]) {
        flexEl.setAttribute(attr[0], '');
      }
    });
  }
  
  ngOnInit(): void {
  }

  constructor() {

  }
}
